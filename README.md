# README #

This README has steps are necessary to get the file-meta application up and running.

### What is this repository for? ###

* This application provides 
	1. Metadata of a file when path to the file location is given as input
	2. List of files/folder within a directory when path to a directory location is given as input
* Version: 1.0

### How do I get set up? ###

1. Clone this project from bitbucket 
	git clone https://schennoj@bitbucket.org/schennoj/file-meta.git
2. Install/Run:
       You can directly run the jar file (springboot jar) already present at this location file-meta/target/file-meta-0.0.1-SNAPSHOT.jar by using the java �jar file-meta-0.0.1-SNAPSHOT.jar command
       OR
	Follow the Deployment instructions (see below). Right Click on the com.meta.FileMetaApplication.java -> Run As -> Java Application
3. Configuration: No configuration needed
4. Dependencies: need Java 8 installed and added to System Path
5. Database configurations: None
6. Deployment instructions: 
 	Import the project into eclipse (preferably) and run the pmo.xml file. Right click on pom.xml-> Run as -> Run Configurations -> enter clean install in Goals field -> Apply -> Run . This will build the project and also run unit test cases

7. How to run unit tests: See the Deployment instructions section

8. Testing the application: 
	Follow the Install/Run step. Open a browser and go to http://localhost:8080  

Enter path to a file in the File Path input file and hit the Search button below it to get file metadata. Try dir1/abc.xml (sample files and directories present in the project jar) or give path to some file on the local storage of the machine (e.g., D:\\somedir\\somefile)

You can enter path to a directory in the Directory Path input file and hit the Search button below it to list of files/directories within the parent directory. Try dir1 (sample files and directories present in the project jar) or give path to some directory on the local storage of the machine (e.g., D:\\somedir)

API Documentation: 

http://localhost:8080/swagger-ui.html 

### Who do I talk to? ###

* Repo owner or admin (schennoj@gmail.com)

