$(document).ready(function () {

    $("#search-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });
    
    $("#dir-form").submit(function (event) {

        //stop submit the form, we will post it manually.
        event.preventDefault();

        dir_ajax_submit();

    });

});

function fire_ajax_submit() {

    var search = {}
    search["username"] = $("#username").val();
    //search["email"] = $("#email").val();

    $("#btn-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/meta/file",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = "<h4>File Info</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#feedback').css("background","green").html(json);

            console.log("SUCCESS : ", data);
            $("#btn-search").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>File Error</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').css("background","red").html(json);

            console.log("ERROR : ", e);
            $("#btn-search").prop("disabled", false);

        }
    });

}

function dir_ajax_submit() {

    var search = {}
    search["dirname"] = $("#dirname").val();
    //search["email"] = $("#email").val();

    $("#dir-search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/meta/dir",
        data: JSON.stringify(search),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

            var json = "<h4>Directory Info</h4><pre>"
                + JSON.stringify(data, null, 4) + "</pre>";
            $('#feedback').css("background-color","green").html(json);

            console.log("SUCCESS : ", data);
            $("#dir-search").prop("disabled", false);

        },
        error: function (e) {

            var json = "<h4>Directory Error</h4><pre>"
                + e.responseText + "</pre>";
            $('#feedback').css("background-color","red").html(json);

            console.log("ERROR : ", e);
            $("#dir-search").prop("disabled", false);

        }
    });

}