package com.metadata.services;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.metadata.model.DirectoryInfo;

@Service
public class DirectoryService {
	Logger logger = LoggerFactory.getLogger(DirectoryService.class);
	public DirectoryInfo getDirectoryInfo(String dirPath) {
		if(dirPath == null || dirPath.isEmpty()) {
			logger.error("Directory path is empty");
		}
		DirectoryInfo di = new DirectoryInfo();
		File f = new File(dirPath);
		Map<String, String> fileMap = new HashMap<String, String>();
		findAllFiles(f, fileMap);
		di.setFiles(fileMap);
		return di;
	}

	private void findAllFiles(File f, Map<String, String> filePathMap) {
		File[] files = f.listFiles();
		for(File file : files) {
			if(file.isDirectory()) {
				findAllFiles(file, filePathMap);
			}
				filePathMap.put(file.getPath(), file.isFile()?"file":"directory");
	}
}
}