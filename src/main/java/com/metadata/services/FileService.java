package com.metadata.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.DosFileAttributes;
import java.nio.file.attribute.PosixFileAttributes;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.directory.BasicAttributes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.metadata.model.DirectoryInfo;
import com.metadata.model.FileInfo;
@Service
public class FileService {

	
//	@Autowired Files files;
//	@Autowired
	BasicFileAttributes attrs;
	Logger logger = LoggerFactory.getLogger(FileService.class);

	public FileInfo getFileInfo(String filePath) {
		DateFormat simple = new SimpleDateFormat("dd MMM yyyy HH:mm:ss:SSS Z"); 
		FileInfo fileInfo = new FileInfo();
	        File f = new File(filePath);
	         String os = System.getProperty("os.name").toLowerCase();
	         fileInfo.setPath(f.getAbsolutePath());
	         fileInfo.setExecutable(f.canExecute());
	         fileInfo.setReadable(f.canRead());
	         fileInfo.setWritable(f.canWrite());
	         fileInfo.setFileName(f.getName());
	         fileInfo.setLastModifiedTime(simple.format(new Date(f.lastModified())));
	         fileInfo.setOperatingSystem(os);
	         System.out.println(os);
	         Path path = Paths.get(filePath);
		        try {
	     		if(os.indexOf("win") >= 0) {
	     			attrs = Files.readAttributes(path, DosFileAttributes.class);
	     		}else {
	     			attrs = Files.readAttributes(path, PosixFileAttributes.class);
	     			PosixFileAttributes posixAttrs = (PosixFileAttributes)attrs;
	     			fileInfo.setGroupName(posixAttrs.group().getName());
	  	           Object[] permissions = posixAttrs.permissions().toArray();
	  	           StringBuilder sb = new StringBuilder();
	  	           for(Object o : permissions) {
	  	        	   sb.append((String) o);
	  	           }
	  	           fileInfo.setPermissions(sb.toString());
	     		}
//	            files.size( path);
	     		int fsize = (int)attrs.size()/1024; 
	        	fileInfo.setSize(fsize + " KB");
	        	fileInfo.setOperatingSystem(os);
	           fileInfo.setDirectory(attrs.isDirectory());
	           fileInfo.setRegularFile(attrs.isRegularFile());
	           fileInfo.setLastAccessTime(simple.format(new Date(attrs.lastAccessTime().toMillis())));
	           fileInfo.setLastModifiedTime(simple.format(new Date(attrs.lastModifiedTime().toMillis())));
	         
	           
			/*
			 * Date result = new Date(attrs.lastAccessTime().toMillis());
			 * System.out.println(simple.format(result));
			 */
	           
	        } catch (NoSuchFileException ignore) {
	        	logger.error("File Not Found", ignore);
	        } 
	          catch (IOException e) {
				logger.error("File Error", e);
			}
	    return fileInfo;
	}
	
}
