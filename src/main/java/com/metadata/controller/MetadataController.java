package com.metadata.controller;

import com.metadata.model.AjaxResponseBody;
import com.metadata.model.DirSearchCriteria;
import com.metadata.model.DirectoryInfo;
import com.metadata.model.FileInfo;
import com.metadata.model.FileSearchCriteria;
import com.metadata.services.DirectoryService;
import com.metadata.services.FileService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/meta")
public class MetadataController {

    
    FileService fs;
    @Autowired 
    DirectoryService ds;
    @Autowired
    public void setFileService(FileService fileService) {
        this.fs = fileService;
    }

    @PostMapping("/file")
    public ResponseEntity<?> getSearchResultViaAjax(@Valid @RequestBody FileSearchCriteria search, Errors errors) {

        AjaxResponseBody result = new AjaxResponseBody();

        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(",")));
            return ResponseEntity.badRequest().body(result);

        }

//        List<User> users = userService.findByUserNameOrEmail(search.getFilename());
        FileInfo fi = fs.getFileInfo(search.getFilename());
        File f = new File(search.getFilename());
        
        if (fi == null) {
            result.setMsg("no file found!");
        } 
        if(f.isDirectory()) {
        	 result.setMsg("This is a directory path");
        }
        else {
            result.setMsg("success");
        }
        
        result.setFileInfo(fi);

        return ResponseEntity.ok(result);

    }

    @PostMapping("/dir")
    public ResponseEntity<?> getDirViaAjax(@Valid @RequestBody DirSearchCriteria search, Errors errors) {

        AjaxResponseBody result = new AjaxResponseBody();

        //If error, just return a 400 bad request, along with the error message
        if (errors.hasErrors()) {

            result.setMsg(errors.getAllErrors().stream().map(x -> x.getDefaultMessage()).collect(Collectors.joining(",")));
            return ResponseEntity.badRequest().body(result);

        }

        File f = null;
        
        if (search.getDirname() == null) {
            result.setMsg("no file found!");
        } else {
        	f = new File(search.getDirname());
        	if(!f.isDirectory()) {
              	 result.setMsg("This is a file path");
              }
               else {
               	DirectoryInfo fi = ds.getDirectoryInfo(search.getDirname());
               	 result.setDirInfo(fi);
                   result.setMsg("success");
               }
        }
        return ResponseEntity.ok(result);

    }
}
