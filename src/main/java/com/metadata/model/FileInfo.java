package com.metadata.model;

import java.util.Date;

public class FileInfo {

	private String fileName;
	private String fileOwner = "";
	private boolean isReadable;
	private boolean isWritable;
	private boolean isExecutable;
	private String size;
	private String fileType = "";
	private String operatingSystem;
	private boolean isDirectory;
	private boolean isRegularFile;
	private String lastAccessTime;
	private String lastModifiedTime;
	private String permissions = "";
	private String groupName = "";
	public String getPath() {
		return path;
	}
	private String path;
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileOwner() {
		return fileOwner;
	}
	public void setFileOwner(String fileOwner) {
		this.fileOwner = fileOwner;
	}
	public boolean isReadable() {
		return isReadable;
	}
	public void setReadable(boolean isReadable) {
		this.isReadable = isReadable;
	}
	public boolean isWritable() {
		return isWritable;
	}
	public void setWritable(boolean isWritable) {
		this.isWritable = isWritable;
	}
	public boolean isExecutable() {
		return isExecutable;
	}
	public void setExecutable(boolean isExecutable) {
		this.isExecutable = isExecutable;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getFileType() {
		return fileType;
	}
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}
	public String getOperatingSystem() {
		return operatingSystem;
	}
	public void setOperatingSystem(String operatingSystem) {
		this.operatingSystem = operatingSystem;
	}
	public boolean isDirectory() {
		return isDirectory;
	}
	public void setDirectory(boolean isDirectory) {
		this.isDirectory = isDirectory;
	}
	public boolean isRegularFile() {
		return isRegularFile;
	}
	public void setRegularFile(boolean isRegularFile) {
		this.isRegularFile = isRegularFile;
	}
	public String getLastAccessTime() {
		return lastAccessTime;
	}
	public void setLastAccessTime(String lastAccessTime) {
		this.lastAccessTime = lastAccessTime;
	}
	public String getLastModifiedTime() {
		return lastModifiedTime;
	}
	public void setLastModifiedTime(String lastModifiedTime) {
		this.lastModifiedTime = lastModifiedTime;
	}
	public String getPermissions() {
		return permissions;
	}
	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}
	public void setPath(String absolutePath) {
		this.path = absolutePath;
		
	}
	
}
