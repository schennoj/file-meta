package com.metadata.model;

import java.util.Map;

public class DirectoryInfo{

	Map<String, String> files;

	public Map<String, String> getFiles() {
		return files;
	}

	public void setFiles(Map<String, String> files) {
		this.files = files;
	}
	
}
