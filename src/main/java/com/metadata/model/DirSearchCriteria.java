package com.metadata.model;

import org.hibernate.validator.constraints.NotBlank;

public class DirSearchCriteria {


@NotBlank(message = "directory  can't empty!")
  String dirname;
  public String getDirname() {
		return dirname;
	}

	public void setDirname(String dirname) {
		this.dirname = dirname;
	}

}