package com.metadata.model;

import java.util.List;

public class AjaxResponseBody {

    String msg;
    FileInfo fileInfo;
    DirectoryInfo dirInfo;

    public DirectoryInfo getDirInfo() {
		return dirInfo;
	}

	public void setDirInfo(DirectoryInfo dirInfo) {
		this.dirInfo = dirInfo;
	}

	public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public FileInfo getFileInfo() {
        return fileInfo;
    }

    public void setFileInfo(FileInfo fileInfo) {
        this.fileInfo = fileInfo;
    }

}
