package com.metadata.model;

import org.hibernate.validator.constraints.NotBlank;

public class FileSearchCriteria {

    @NotBlank(message = "filename can't empty!")
    String filename;

	public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
}