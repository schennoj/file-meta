package com.metadata;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FileMetaApplication {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(FileMetaApplication.class, args);
    }

}