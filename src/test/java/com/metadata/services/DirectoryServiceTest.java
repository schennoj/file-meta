package com.metadata.services;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.metadata.model.DirectoryInfo;

//@RunWith(SpringRunner.class)
@SpringBootTest
public class DirectoryServiceTest {

	@Autowired
	DirectoryService ds;
	@Test
	public void testDirectoryInfo() {
		DirectoryInfo di = ds.getDirectoryInfo("dir1");
		Map<String,String> files = di.getFiles();
		assertNotNull(files);
	}

}
