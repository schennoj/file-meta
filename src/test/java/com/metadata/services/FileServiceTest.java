package com.metadata.services;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.metadata.model.FileInfo;

//@RunWith(SpringRunner.class)
@SpringBootTest
public class FileServiceTest {

	@Autowired
	FileService fs;
	
	@Test
	public void testFileInfo() {
		FileInfo fi = fs.getFileInfo("dir1/abc.xml");
		assertNotNull(fi.getPath());
	}

}
