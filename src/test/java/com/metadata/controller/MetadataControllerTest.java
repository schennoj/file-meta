package com.metadata.controller;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.net.URI;
import java.net.URISyntaxException;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import com.metadata.model.DirSearchCriteria;
import com.metadata.model.FileSearchCriteria;
 
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class MetadataControllerTest 
{
    @Autowired
    private TestRestTemplate restTemplate;
     
    @LocalServerPort
    int randomServerPort;
 
    @Test
    public void testFileSearch() throws URISyntaxException 
    {
        final String baseUrl = "http://localhost:"+randomServerPort+"/meta/file";
        URI uri = new URI(baseUrl);
        FileSearchCriteria fsc = new FileSearchCriteria();
        fsc.setFilename("dir1/abc.xml");
         
        HttpHeaders headers = new HttpHeaders();
        headers.set("X-COM-PERSIST", "true");      
 
        HttpEntity<FileSearchCriteria> request = new HttpEntity<>(fsc, headers);
         
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
         
        //Verify request succeed
        assertEquals(200, result.getStatusCodeValue());
    }
     
    @Test
    public void testDirSearch() throws URISyntaxException 
    {
        final String baseUrl = "http://localhost:"+randomServerPort+"/meta/dir";
        URI uri = new URI(baseUrl);
        DirSearchCriteria dsc = new DirSearchCriteria();
         dsc.setDirname("dir1");
        HttpHeaders headers = new HttpHeaders();
 
        HttpEntity<DirSearchCriteria> request = new HttpEntity<>(dsc, headers);
         
        ResponseEntity<String> result = this.restTemplate.postForEntity(uri, request, String.class);
         
        //Verify bad request and missing header
        assertEquals(200, result.getStatusCodeValue());
    }
 
}